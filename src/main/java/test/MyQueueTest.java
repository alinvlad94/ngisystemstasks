package test;


import model.Pacient;
import org.junit.Test;

import org.testng.annotations.BeforeMethod;
import queue.HasPriority;
import queue.MyQueue;

import static org.junit.Assert.*;

/**
 * Created by vlada on 27/07/2016.
 */

public class MyQueueTest {

    MyQueue<Pacient> queueOfPacients;

    @BeforeMethod
    public void setup(){
    }

    @Test
    public void testInsertAndExtractElementsInOrder(){

        queueOfPacients = new MyQueue<>();
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 5));
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 1));
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 4));
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 3));
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 2));
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 1));
        insertSpecificElement(queueOfPacients, new Pacient("Alin", "Vlad", 1));

        assertEquals(1, extractElement(queueOfPacients).getPriority());
        assertEquals(1, extractElement(queueOfPacients).getPriority());
        assertEquals(1, extractElement(queueOfPacients).getPriority());
        assertEquals(2, extractElement(queueOfPacients).getPriority());
        assertEquals(3, extractElement(queueOfPacients).getPriority());
        assertEquals(4, extractElement(queueOfPacients).getPriority());
    }

    @Test
    public void testTheEnqueueMethod(){

        queueOfPacients = new MyQueue<>();
        assertEquals(0, queueOfPacients.length);
        insertElement(queueOfPacients);
        assertEquals(1, queueOfPacients.length);
    }

    @Test
    public void testTheDequeueMethod(){

        queueOfPacients = new MyQueue<>();
        insertElement(queueOfPacients);
        insertElement(queueOfPacients);
        Pacient pacient = (Pacient) extractElement(queueOfPacients);
        assertEquals("Alin", pacient.getNume());
        assertEquals("Vlad", pacient.getPrenume());
        assertEquals(1, pacient.getPriority());
    }

    public void insertElement(MyQueue queueOfPacients){
        queueOfPacients.enqueue(new Pacient("Alin", "Vlad", 1));
    }

    public void insertSpecificElement(MyQueue queueOfPacients, Pacient pacient){
        queueOfPacients.enqueue(pacient);
    }

    public HasPriority extractElement(MyQueue queueOfPacients){
        return queueOfPacients.dequeue();
    }
}
