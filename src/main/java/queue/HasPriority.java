package queue;

/**
 * Created by vlada on 27/07/2016.
 * Interface
 * Used to make possible the extraction of the priority of the T object in the the -enqueue() method
 * This interface make sure that every element of the queue implements the method getPriority
 * We can only use the MyQueue to insert objects instantited from classes that implements this interface
 */
public interface HasPriority {
    /**
     *
     * @return the priority of the element
     */
    int getPriority();
}
