package queue;

/**
 * Created by vlada on 27/07/2016.
 * Generic Class
 * Priority Queue
 * Allows only classes that implements the HasPriority Interface
 */
public class MyQueue<T extends HasPriority> {

    public MyQueueElement<T> head = null;
    public MyQueueElement<T> tail = null;

    public int length = 0;

    /**
    * @param T(generic) element that we want to insert into the queue
     *           the element will be inserted based on his priority
     *           1 is the highest priority
     *           the queue is filling up from the tail in the direction of the head
     **/
    public void enqueue(T element){
        MyQueueElement<T> newElement = new MyQueueElement<>(element);

        if(length == 0 ){
            head = tail = newElement;
            length++;
        } else if(length == 1){
            if(compare(newElement.getPriority(), head.getPriority()) >= 0){
                head = newElement;
                head.next = tail;
                tail.prev = head;
                length++;
            }
            else{
                tail = newElement;
                head.next = tail;
                tail.prev = head;
                length++;
            }
        }else {
            insertInRightPlace(newElement);
        }
    }

    /**
     *
     * @param newElement the element that we want to add
     * @return boolean to know that the newElement was succefully added into the queue
     * this method has the logic that insert the newElement into the queue based on the priority
     */
    private boolean insertInRightPlace(MyQueueElement newElement) {
        MyQueueElement<T> iterator = head;
        while(iterator != null){
            if(compare(newElement.getPriority(), iterator.getPriority())< 0){
                if(iterator.next == null) {
                    newElement.prev = tail;
                    tail.next = newElement;
                    tail = newElement;
                    length++;
                    return true;
                }
                iterator = iterator.next;
            }else{
                iterator.prev.next = newElement;
                newElement.prev = iterator.prev;
                iterator.prev = newElement;
                newElement.next = iterator;
                length++;
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param element is the newElement that we want to insert into the queue
     *                This method add new elements in the queue not based on their priority
     *                Adds element in the direction from the tail to the head
     *                This method gives the functionality of a normal Queue ( FIFO ), ( the older element in the queue is the first element to be extracted)
     */
    public void add(T element){
        MyQueueElement newElement = new MyQueueElement<T>(element);
        if(length == 0){
          head = tail = newElement;
            length++;
        }else if(length == 1){
          head = newElement;
            head.next = tail;
            tail.prev = head;
            length++;
        }
        else {
            head.prev = newElement;
            newElement.next = head;
            head = newElement;
            length++;
        }
    }


    /**
     *
     * @return T generic class
     *         this method extract the tail of the queue
     */
    public T dequeue(){
        MyQueueElement<T> tailCopy = tail;
        tail = tail.prev;
        tail.next = null;
        length--;
        return tailCopy.obj;
    }

    /**
     *
     * @param p1 integer - first number
     * @param p2 integer - second number
     * @return integer values that we use in if statements in enqueue() method
     */
    private int compare(int p1, int p2) {
        if (p1 > p2) {
            return 1;
        } else if (p1 == p2) {
            return 0;
        } else {
            return -1;
        }
    }
}
