package queue;

/**
 * Created by vlada on 27/07/2016.
 * Generic element of the MyQueue ( Priority Queue )
 */
public class MyQueueElement<T extends HasPriority> {

    /**
     * Because we used a double linked list, every element of the queue points to the previous element and to the one after him
     */
    public MyQueueElement<T> next = null;
    public MyQueueElement<T> prev = null;
    T obj;

    /**
     * Constructor
     * @param obj
     */
    public MyQueueElement(T obj){
        this.obj = obj;
    }

    /**
     *
     * @return the priority of the object
     */
    public int getPriority() {
        return obj.getPriority();
    }
}
