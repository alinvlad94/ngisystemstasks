package model;

import queue.HasPriority;


/**
 * Created by vlada on 27/07/2016.
 * Class created to demonstrate the use of the queue
 */
public class Pacient implements HasPriority{

    /**
     * To these fields can be also added the problem of the pacients, or other infos...
     */
    private String nume;
    private String prenume;

    /**
     * Based on this attribute the pacients are added into a special order into the queue,
     * so that the nurse can send them to the doctor in a specific order ( priority order)
     */
    private int priority;

    /**
     * Constructor
     * @param nume
     * @param prenume
     * @param priority
     */
    public Pacient(String nume, String prenume, int priority) {
        this.nume = nume;
        this.prenume = prenume;
        this.priority = priority;
    }

    public String getNume() {
        return nume;
    }

    public Pacient setNume(String nume) {
        this.nume = nume;
        return this;
    }

    public String getPrenume() {
        return prenume;
    }

    public Pacient setPrenume(String prenume) {
        this.prenume = prenume;
        return this;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
