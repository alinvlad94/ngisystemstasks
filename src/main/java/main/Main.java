package main;


import model.Pacient;
import queue.MyQueue;

/**
 * Created by vlada on 27/07/2016.
 * Exemple of use ( Doctor,Pacients and Nurse exemple)
 */
public class Main {

    public static void main(String[] args){

        MyQueue<Pacient> queue = new MyQueue<Pacient>();

        Pacient p1 = new Pacient("Alin","Vlad",5);
        Pacient p6 = new Pacient("Alin","Vlad",1);
        Pacient p2 = new Pacient("Alin","Vlad",4);
        Pacient p3 = new Pacient("Alin","Vlad",3);
        Pacient p4 = new Pacient("Alin","Vlad",2);
        Pacient p5 = new Pacient("Alin","Vlad",1);

        queue.enqueue(p1);
        queue.enqueue(p6);
        queue.enqueue(p2);
        queue.enqueue(p3);
        queue.enqueue(p4);
        queue.enqueue(p5);
        System.out.println(queue.length);


        System.out.print(queue.dequeue().getPriority()+"  ");
        System.out.print(queue.dequeue().getPriority()+"  ");
        System.out.print(queue.dequeue().getPriority()+"  ");
        System.out.print(queue.dequeue().getPriority()+"  ");
        System.out.print(queue.dequeue().getPriority()+"  ");

    }
}
